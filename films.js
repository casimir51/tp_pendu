
let words = ["seven", "titanic", "terminator", "avengers", "superman"];
let alphabet = ["A","Z","E","R","T","Y","U","I","O","P","Q","S","D","F","G","H","J","K","L","M","W","X","C","V","B","N"]
let imageTab = ['./assets/image0.jpg', './assets/image1.jpg', './assets/image2.jpg', './assets/image3.jpg', './assets/image4.jpg', './assets/image5.jpg', './assets/image6.jpg', './assets/image7.jpg', './assets/image8.jpg']

const clavier = document.querySelector('#clavier')
const usedContainerHtml = document.querySelector('.usedContainer')
const hideHtml = document.querySelector('#hide')
const drawHtml = document.querySelector('#draw')
const catchHtml = document.querySelector('.catch')

var usedLetter = []
var life = 7
const rand = Math.round(Math.random() * words.length)

const initWord = words[rand].toUpperCase()
const wordtoArray = initWord.split('')
var hideTab = wordToUnd(wordtoArray)

for (let i = 0; i < alphabet.length; i++) {
  clavier.innerHTML += '<button class="btn btn-secondary btn m font50 animated" id="' + alphabet[i] + '" onclick=run("' + alphabet[i] + '")>' + alphabet[i] + '</button>'
}


infoGame(hideTab, usedLetter, life, '')

function wordToUnd(word){
  let tabU = word.reduce((acc, _) => acc.concat('_') ,[])
  return tabU
}

function run(letter){
  document.querySelector("#" + letter).setAttribute('disabled', true)
  document.querySelector("#" + letter).classList.remove('btn-primary')
  let isInW = isInWord(letter, hideTab, wordtoArray)
  if (!isInW) {
    document.querySelector("#" + letter).classList.add('btn-danger')
    document.querySelector("#" + letter).classList.add('flipOutX')
    usedLetter.push(letter)
    life--
  }else{
    document.querySelector("#" + letter).classList.add('btn-success')
    document.querySelector("#" + letter).classList.add('pulse')
    letter = ''
  }
  infoGame(hideTab, usedLetter, life, letter)
}

function isInWord(letter, hideTab, wordtoArray){
  var isIn = false
  for (let i = 0; i < wordtoArray.length; i++) {
    if (letter === wordtoArray[i]) {
      isIn = true
      hideTab[i] = wordtoArray[i]
    }
  }
  return isIn
}

function infoGame(hideTab, usedLetter, life, letter){
  hideHtml.innerHTML = hideTab.join(' ')
  let image = imageTab[life]
  drawHtml.innerHTML = '<img src="'+image+'" alt="hangman" />'
  if (letter.length) {
    catchHtml.innerHTML = '<button class="btn btn-danger font50 m animated flipInX">' + letter + '</button>'  
    setTimeout(function(){usedContainer(usedLetter)}, 1000);
  }
  if (life === 0) {
    hideHtml.innerHTML = 'Perdu le mot était : ' + initWord
    clavier.innerHTML = '<button class="btn btn-warning rubberBand text-white animated" onclick=document.location.reload()>Rejouer</button>'
  }
  if (!hideTab.includes('_')) {
    hideHtml.innerHTML = 'Gagné le mot était : ' + initWord
    clavier.innerHTML = '<button class="btn btn-outline-warning zoomIn text-white fast animated" onclick=document.location.reload()>Rejouer</button>'
  }
}

function usedContainer(usedLetter){
  usedContainerHtml.innerHTML = ''
  for (let i = 0; i < usedLetter.length; i++) {
    usedContainerHtml.innerHTML += '<button class="btn btn-danger font50 m">' + usedLetter[i] + '</button>'
  }
  catchHtml.innerHTML = ''  
}

function makeProgress(){
for (let i = 0; i < wordtoArray.length; i++) {
       $(".progress-bar").css("width", i + "%").text(i + " %");
            }
            setTimeout("makeProgress()",);
        }
        makeProgress();



